import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../app/rest.service';
@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  user = [];

  constructor(private dataService: RestService) { }
  
  ngOnInit() {
    
    
    this.dataService.sendGetRequest().subscribe((data: any[])=>{
      //console.log(data);
      this.user = data;
    }) 

    

    
    
  }
  

}
