import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { DefaultComponent } from './components/default.component';
import { HttpClientModule } from '@angular/common/http';
import {SharedModule} from '../shared/shared.module';
@NgModule({
  declarations: [DefaultComponent],
  imports: [
    CommonModule,
    ServiceRoutingModule,
    SharedModule,
    HttpClientModule,
  ]
})
export class ServiceModule { }
