import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DefaultComponent} from './components/default.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    data: {
      breadcrumb: 'Service',
      icon: 'icofont icofont-file-document bg-c-pink',
      breadcrumb_caption: 'Add',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
