import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BasicBootstrapComponent} from './basic-bootstrap.component';

const routes: Routes = [
  {
    path: '',
    component: BasicBootstrapComponent,
    data: {
      breadcrumb: 'Inventory Management',
      icon: 'icofont-table bg-c-blue',
      breadcrumb_caption: 'List all inventory in our system',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicBootstrapRoutingModule { }
