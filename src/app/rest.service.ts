import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RestService {
  private REST_API_SERVER = "https://mocki.io/v1/fa4c31fb-7cec-4840-9b75-a7516a7a6ac4";
  private REST_API_SERVER_ITENTIAL = "https://datacomm-poc-iap.trial.itential.io/configuration_manager/devices?token=";
  private token = 'NTYxNzIyZWRlZTI5ZmQ1NTc4NjlkMDM4NDUyMTFiMzk=';
  constructor(private httpClient: HttpClient) {

  }
  public sendGetRequest() {
    return this.httpClient.get(this.REST_API_SERVER);
  }

  public getDevices() {
    return this.httpClient.post<any>(this.REST_API_SERVER_ITENTIAL + this.token,
      {
        "options": {
          "limit": 100,
          "start": 0,
          "sort": [
            {
              "name": 1,
              "address": -1,
              "port": 1
            }
          ],
          "order": "ascending"
        }
      });
  }

}
