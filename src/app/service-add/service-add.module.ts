import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceAddRoutingModule } from './service-add-routing.module';
import { DefaultComponent } from './components/default.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [DefaultComponent],
  imports: [
    CommonModule,
    ServiceAddRoutingModule,
    SharedModule
  ]
})
export class ServiceAddModule { }
